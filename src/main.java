import com.googlecode.javacv.cpp.opencv_core;
import lcm.lcm.LCM;
import lcm_serial_types.Actuators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.CV_LOAD_IMAGE_GRAYSCALE;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

public class main {

    // поля класса
    final static int magicProportion = 32;     // магия для корректного отображения результатов
    static int razmerPolya = 33267;            // характерный размер поля
    static int N = 8;                          // количество заданных движений
    static int numOfVelositiTypes = 0 ;        // количество концентрических окружностей
    static int numOfAngleVelosityTypes = 0;    // количество секторов
    static int maxVelosity =(int)(50 -32767*50/32767) ;
    static int maxAngleVelisity = 0;
    static int colourOfJoisticField = 0;

    // поля для вычисления длины траектории джойстика
    static int dlinaTraectoriiJoisticka = 0;
    static int xJoyPred = 0;
    static int yJoyPred = 0;
    static int odo = 0;
    static int sunOtnUscorenie = 0;
    static int radiuspovorota = 0;
    static int frameSiseType = 0;

    static int velosity = 0;
    static double angleVelosity = 0;
    static int xTanka = 0;
    static int yTanka = 0;

    static String levelOfPilot = "";
    static double maxRadius = 200;                // радиус максимального отклонения джойстика
    static int maxRadiusPriv = (int)maxRadius*magicProportion; // приведенное значение максимального радиуса
    static int xJoy = 0;                       // координаты джойстика относительно положения покоя
    static int yJoy = 0;                       // координаты джойстика относительно положения покоя
    static int [][] joystickParametr = new int[1000][1000]; // надо уточнить количество типов и каналов
    static int time = 0;                          // круговые координаты курсора
    static int timeAbs = 0;
    static double rad = 57.2;
    static String[] soursePath = new String[N];// массив для путей к правильным траекториям
    static opencv_core.IplImage img;           // исходная картинка (белое поле)
    static opencv_core.IplImage imgPodlojka;   // подложка, необходимая для обновления картинки
    public static int xPloshForRis;            // координаты начальной площадки
    public static int yPloshForRis;            // координаты начальной площадки
    public static boolean rabota = true;
    public static boolean ruchnoyRegim = false;
    private static String controlMode = "SMART SYSTEM";

    public static void main(String[] args) throws FileNotFoundException {
        LCM lcm;
        Actuators serial_actuators;
        try {  
            lcm = new LCM("udpm://239.255.76.67:7667?ttl=0");
            serial_actuators = new Actuators();
        } catch (IOException ex) {
            lcm = null;
            serial_actuators = null;
            System.out.println("Exception: " + ex);
        }

       img = cvLoadImage("fonimg.jpeg", CV_LOAD_IMAGE_GRAYSCALE) ;      // загружаем картинку
       imgPodlojka = cvLoadImage("fonimg.jpeg", CV_LOAD_IMAGE_GRAYSCALE) ;  // загружаем картинку подложки
       final int xPloshadki =  (img.width()/2)*magicProportion - (int)maxRadius*magicProportion;             // координаты начальной площадки
       final int yPloshadki =  (img.height()/2)*magicProportion;            // координаты начальной площадки
       xPloshForRis = (img.width()/2)*magicProportion - (int)maxRadius*magicProportion;                      // координаты начальной площадки
       yPloshForRis = (img.height()/2)*magicProportion;                     // координаты начальной площадки

        // считываем основные параметры поля
      soursePath[0] =  "src/settings.txt";
      Scanner in = new Scanner(new File(soursePath[0]));
      while(in.hasNext())
       {
            numOfVelositiTypes = in.nextInt() ;          // количество различных типов линейных скоростей
            numOfAngleVelosityTypes = in.nextInt();      // количество различных типов угловых скоростей
            maxVelosity = in.nextInt();
            maxAngleVelisity = in.nextInt();
        }
        in.close();

      final risovalka risovalka1 = new risovalka();    // создаем рисовалку всего, что на поле, кроме точки курсора
      final shower shower1 = new shower();             // создаем показыватель картинки
      final poligon poligon1 = new poligon();
      risovalka1.risovanie(xJoy,yJoy);                          // рисуем
      poligon1.makePoligon();                         // рисуем полигон
        poligon1.makeObstracles();

        // Создаем драйвер для джойстика и вытаскиваем из него инфу о координатах
        // Time - время работы джойстика, оно абсолютное 0_o
        //

        GameControlDriver GameController1 = new GameControlDriver("/dev/input/js0") ;
        GameController1.addGameEventListener(new GameControlDriver.GameEventListener() {
                           //   @Override
                                  public void gameEventReceived(long time, int type, int channel, int value) {

                         cvLine(img, cvPoint(xJoy,yJoy),cvPoint(xJoy,yJoy), CV_RGB(255, 255, 255), 5, 5, 5); // затираем указатель
                         cvLine(img, cvPoint(xJoy,yJoy),cvPoint(xJoy,yJoy), CV_RGB(0, 0, 0), 3, 3, 5); // ставим жирную черную точку
                         timeAbs = (int)time;
                         joystickParametr[type][channel] = value;
                         xJoy = (int)(joystickParametr[2][0]*maxRadius*magicProportion/razmerPolya) + xPloshadki;
                         yJoy = (int)(joystickParametr[2][1]*maxRadius*magicProportion/razmerPolya) + yPloshadki;

                         dlinaTraectoriiJoisticka = dlinaTraectoriiJoisticka  + ((int)Math.sqrt(Math.pow(xJoy-xJoyPred, 2) + Math.pow(yJoy-yJoyPred, 2)))/100;
                         maxVelosity = (int)(70 -(double)(joystickParametr[2][3]*100/32767));
                         xJoyPred = xJoy;
                         yJoyPred = yJoy;

                         // мощность движка

                         if (joystickParametr[1][2] == 1){
                            if (ruchnoyRegim == true) {
                                ruchnoyRegim = false;
                                controlMode = "SMART SYSTEM";
                                colourOfJoisticField = 0;
                            }  else {
                                ruchnoyRegim = true;
                                controlMode = "MANUAL";
                                colourOfJoisticField = 255;
                            }
                         }
                         if (joystickParametr[1][11] == 1) {
                             frameSiseType ++;
                             if (frameSiseType == 3){
                                 frameSiseType =0;
                             }
                             if (frameSiseType == 0){

                             }



                         }

                         if (joystickParametr[1][4] == 1) {
                             if (colourOfJoisticField == 0){
                                 colourOfJoisticField = 255;
                             } else {
                                 colourOfJoisticField = 0;
                             }

                         }


                         System.out.println("time=" + time + " type=" + type + " channel=" + channel + " value=" + value);

                                     }

        });

        if (numOfVelositiTypes*numOfAngleVelosityTypes == 9) levelOfPilot  = "LOW";
        if (numOfVelositiTypes*numOfAngleVelosityTypes == 25) levelOfPilot = "MEDIUM";
        if (numOfVelositiTypes*numOfAngleVelosityTypes == 49) levelOfPilot = "ADVANCED";
        if (numOfVelositiTypes*numOfAngleVelosityTypes == 81) levelOfPilot = "PERFECT";

        // основной цикл в этой проге
        do{

          //  вывод текстовой информации
          cvPutText(main.img,"time: " + String.valueOf(main.time),cvPoint(10,10),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"velocity: " + String.valueOf(main.velosity),cvPoint(10,20),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"angle velocity: " + String.valueOf((int)main.angleVelosity),cvPoint(10,30),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"level of the pilot: " + levelOfPilot,cvPoint(120,10),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"control mode: " + controlMode , cvPoint(120,20),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"joystick trajectory length: " + String.valueOf(dlinaTraectoriiJoisticka) , cvPoint(120,30),cvFont(0.7, 1) ,CvScalar.BLACK);
          cvPutText(main.img,"vehicle trajectory length: " + odo , cvPoint(340,10),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"total relative acceleration: " + sunOtnUscorenie , cvPoint(340,20),cvFont(0.7,1) ,CvScalar.BLACK);
          cvPutText(main.img,"radius of turn: " + radiuspovorota , cvPoint(340,30),cvFont(0.7,1) ,CvScalar.BLACK);




            time++;
           cvLine(img, cvPoint(xJoy,yJoy),cvPoint(xJoy,yJoy), CV_RGB(0, 0, 0), 5, 5, 5); // ставим жирную черную точку
           risovalka1.risovanie(xJoy , yJoy);
            if (lcm != null && serial_actuators != null) {
                serial_actuators.motor_speed_left = (short) (4 * velosity);
                serial_actuators.motor_speed_right = (short) (4 * velosity);          
                serial_actuators.motor_speed_left -= angleVelosity / 2;
                serial_actuators.motor_speed_right += angleVelosity / 2;
                lcm.publish("SERIAL_ACTUATORS", serial_actuators);
            }
           shower1.showImage(img);


       //  System.out.print(time + "\n");


         // затирание неактуальной текстовой информации
         cvPutText(main.img,"time: " + String.valueOf(main.time),cvPoint(10,10),cvFont(0.7,30) ,CvScalar.WHITE);
         cvPutText(main.img, "velocity: " + String.valueOf(main.velosity), cvPoint(10, 20), cvFont(0.7, 30), CvScalar.WHITE);
         cvPutText(main.img,"angle velocity: " + String.valueOf((int)main.angleVelosity),cvPoint(10,30),cvFont(0.7,30) ,CvScalar.WHITE);
         cvPutText(main.img,"level of the pilot: " + levelOfPilot,cvPoint(120,10),cvFont(0.7,30) ,CvScalar.WHITE);
         cvPutText(main.img,"control mode: SMART SYSTEM" , cvPoint(120,20),cvFont(0.7, 30) ,CvScalar.WHITE);
         cvPutText(main.img,"Joystick trajectory length" + String.valueOf(dlinaTraectoriiJoisticka) , cvPoint(120,30),cvFont(0.7, 30) ,CvScalar.WHITE);
         cvPutText(main.img,"vehicle trajectory length: " + odo , cvPoint(340,10),cvFont(0.7,30) ,CvScalar.WHITE);
         cvPutText(main.img,"total relative acceleration: " + sunOtnUscorenie , cvPoint(340,20),cvFont(0.7,30) ,CvScalar.WHITE);
         cvPutText(main.img,"radius of turn: " + radiuspovorota , cvPoint(340,30),cvFont(0.7,30) ,CvScalar.WHITE);

            poligon1.makePoligon();

        } while (rabota == true) ;

          //  cvWaitKey(0);



        // ужасно навороченный обработчик действий мыши который теперь нафиг не нужен(
   /*
    final CvMouseCallback on_mouse = new CvMouseCallback() {
        @Override
        public void call(int event, int x, int y, int flags, com.googlecode.javacpp.Pointer param) {    // метод движения мыши


            switch( event ){
                case CV_EVENT_MOUSEMOVE:  {                              // действия при движении мыши

                    r = (int)Math.sqrt(Math.pow(xPloshadki - x,2) + Math.pow(yPloshadki - y,2));    // радиус курсора в круговых координат

                    if (r !=0){

                        if (-yPloshadki + y < 0 ){      // угол курсора в круговых координатах

                            fi = Math.acos((double)(-xPloshadki + x)/(double)r)*rad;

                        }   else {

                            fi =  360 - Math.acos((double)(-xPloshadki + x)/(double)r)*rad;


                        }

                    }

                    cvLine(main.img, cvPoint(x * magicProportion, y * magicProportion),    //ставим жирную точечку
                            cvPoint(x * magicProportion, y * magicProportion), CV_RGB(0, 0, 0), 10, 10, 5);

                    cvShowImage("LKpyr_OpticalFlow", img);
                    risovalka1.risovanie();

                    break;
                }

                case CV_EVENT_LBUTTONDOWN: {

                    break;
                }

                case CV_EVENT_LBUTTONUP:
                    turn =1;
                    break;

                case CV_EVENT_RBUTTONDOWN: {

                    break;
                }
                case CV_EVENT_RBUTTONUP:
                    break;

                }

        }
    };
          cvSetMouseCallback("LKpyr_OpticalFlow",on_mouse,null);              // заряжаем движение мышки
  */

    }


}

