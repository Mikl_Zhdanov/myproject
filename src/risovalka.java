/*
Рисование
Необходимо для повторного отображения поля, а также для очистки поля от предидущих траекторий
 */
import static com.googlecode.javacv.cpp.opencv_core.*;

public class risovalka {
   // поля класса
   static int magicProportion = 32; // вдруг пригодится
   static int  numOfVelositiTypes = main.numOfVelositiTypes;
   static int  numOfAngleVelosityTypes = main.numOfAngleVelosityTypes;
   static int xPloshadki = main.xPloshForRis;
   static int yPloshadki = main.yPloshForRis;
   static double maxRadius = main.maxRadiusPriv;
   static double  widthPolya = maxRadius/numOfAngleVelosityTypes*2;
   static double  heightPolua = maxRadius/numOfVelositiTypes*2;
   static int[][] xPolya = new int[numOfVelositiTypes][numOfAngleVelosityTypes];
   static int[][] yPolya = new int[numOfVelositiTypes][numOfAngleVelosityTypes];
   static int nomerPolya = 0;
   static int nomerProshlogoPolya = 0;
   static int deltaTime = 0;
   static int previosTime = 0;
   static int reactionTime = 10/(numOfVelositiTypes*numOfAngleVelosityTypes);
   static int[] velociti = new int [main.numOfVelositiTypes];
   static int[] angleVelositi = new int [main.numOfAngleVelosityTypes];
   static boolean novoePole = false;

   static int equilibriumX = 8960;
   static int equilibriumY = 7680;
   static int maxX =15263;
   static int maxY =15263;


   // повторное рисование поля
    void risovanie (int xJoy, int yJoy) {


        poligon poligon2 = new poligon();

       // рисуем поле скоростей (разбиение на поля)
        for(int i = 0; i< numOfVelositiTypes; i++) {
            for(int j = 0; j < numOfAngleVelosityTypes; j++) {

           xPolya[i][j] = xPloshadki - (int)maxRadius + (int)(maxRadius*2/numOfAngleVelosityTypes*j);    // координаты каждошго поля
           yPolya[i][j] = yPloshadki - (int)maxRadius +(int)(maxRadius*2/numOfVelositiTypes*i);          // координаты каждошго поля
          // очищаем картинку от предидущего шлака
           cvRectangle(main.img,cvPoint(xPolya[i][j], yPolya[i][j]),
              cvPoint(xPolya[i][j] + (int)widthPolya,yPolya[i][j] + (int)heightPolua), CV_RGB(255, 255, 255), 3, 3, 5);
          // заполняем картинку полями скоростей
           cvRectangle(main.img,cvPoint(xPolya[i][j], yPolya[i][j]),
                   cvPoint(xPolya[i][j] + (int)widthPolya,yPolya[i][j] + (int)heightPolua),
                   CV_RGB(main.colourOfJoisticField,main.colourOfJoisticField, main.colourOfJoisticField), 1, 1, 5);

           // System.out.print(xJoy +"\n");
                cvRectangle(main.img, cvPoint(xPloshadki - (int) maxRadius, yPloshadki - (int) maxRadius),
                        cvPoint(xPloshadki + (int) maxRadius, yPloshadki + (int) maxRadius), CV_RGB(0, 0, 0), 1, 1, 5);
        }
     }

        if (main.ruchnoyRegim == true){



           main.angleVelosity =  - (double)main.maxAngleVelisity*((double)(main.xJoy - equilibriumX))/maxX;     //(main.xJoy - 2657)*main.maxVelosity/15263 ;
           main.velosity = -(main.yJoy - equilibriumY)*main.maxVelosity/maxY ;
           poligon2.dvigenie(main.velosity,main.angleVelosity);


        }


        for(int i = 0; i< numOfVelositiTypes; i++) {
            for(int j = 0; j < numOfAngleVelosityTypes; j++) {
                nomerPolya++;

                velociti[i] = (int)(((double)main.maxVelosity + (double)main.maxVelosity/(double)numOfVelositiTypes)/2
                        - (double)main.maxVelosity/(double)numOfVelositiTypes*(i + 1));
                angleVelositi[j] = (int)(((double)main.maxAngleVelisity + (double)main.maxAngleVelisity/(double)numOfAngleVelosityTypes)/2
                        - (double)main.maxAngleVelisity/(double)numOfAngleVelosityTypes*(j + 1));

                if (xJoy > xPolya[i][j] && xJoy < xPolya[i][j] + (int)widthPolya) {
                    if (yJoy > yPolya[i][j] && yJoy < yPolya[i][j] + (int)heightPolua ) {

                        if (nomerPolya != nomerProshlogoPolya){
                            novoePole = true;
                            previosTime = main.time;
                            nomerProshlogoPolya = nomerPolya;
                            novoePole = false;
                        }

                        if (deltaTime > reactionTime && main.ruchnoyRegim == false) {

                        deltaTime = 0;
                        poligon2.dvigenie(velociti[i],angleVelositi[j]);
                        cvRectangle(main.img, cvPoint(xPolya[i][j], yPolya[i][j]),
                            cvPoint(xPolya[i][j] + (int) widthPolya, yPolya[i][j] + (int) heightPolua),
                            CV_RGB(main.colourOfJoisticField, main.colourOfJoisticField, main.colourOfJoisticField), 3, 3, 5);
                        main.velosity = velociti[i];
                        main.angleVelosity = angleVelositi[j];
                        }



                    }

                }


            }

        }


        deltaTime = main.time - previosTime;
        nomerPolya = 0;


    }

}
