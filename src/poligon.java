import static com.googlecode.javacv.cpp.opencv_core.*;

public class poligon {

    static double fi = -90;  // направление машинки
    static double radian = 57.29;  // градусов в радиане
    static int magicProportion = main.magicProportion;
    static int radius = (int)main.maxRadius*magicProportion;
    static int xPoligona = main.xPloshForRis + 2*radius + 1000;
    static int yPoligona = main.yPloshForRis;
    static int xTanka;
    static int yTanka;
    static int xTankaPred;
    static int yTankaPred;
    static int xTankaAbs = xPoligona + xTanka;
    static int yTankaAbs = yPoligona + yTanka;
    static int vectorSkorosty = 500;
    static int machineLenth = 1000;
    static int machineWidth  = 500;
    static int machineDiagonal = (int)Math.sqrt(Math.pow(machineWidth/2,2)+Math.pow(machineLenth/2,2));
    static double machineAngle = Math.atan((double)machineWidth/machineLenth);
    static  int numberOfObstacles = 3;
    static int radiusObstracles = 500;
    static int x = 0;
    static int y = 0;
    static int odo = 0;
    static int sumOtnUsk = 0;
    static int sumUglovoeUsk = 0;
    static int sumLinUsk  = 0;
    static int radiusPovarota = 0;
    static int oldVelosity = 0;
  public void makePoligon(){

      cvRectangle(main.img, cvPoint(xPoligona - (int) radius, yPoligona - (int) radius),
              cvPoint(xPoligona + (int) radius, yPoligona + (int) radius), CV_RGB(0, 0, 0), 1, 1, 5);


  }

  public void makeObstracles(){

      for (int i = 0; i< 3; i++){
          for (int j = 0; j< 3; j++){
          x = xPoligona - 3000 + 3000*i ;
          y = yPoligona -4000 + 4000*j;

          if( i != 1 || j != 1) cvRectangle(main.img, cvPoint(x - (int) radiusObstracles, y - (int) radiusObstracles),
                  cvPoint(x + (int) radiusObstracles,y + (int) radiusObstracles), CV_RGB(0, 0, 0), 1, 1, 5);
          }
      }


  }


  public void dvigenie(int velositi, double angleVelosity){


      // стенки препятствий


              // Затирка танчика
      cvLine(main.img,cvPoint(xTankaAbs, yTankaAbs),cvPoint(xTankaAbs + (int)(vectorSkorosty*Math.cos(fi/radian))
               ,yTankaAbs + (int)(vectorSkorosty*Math.sin(fi/radian))),CV_RGB(255, 255, 255), 2, 2, 5);

      cvLine(main.img,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(255, 255, 255), 1, 1, 5);

      cvLine(main.img,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(255, 255, 255), 1, 1, 5);

      cvLine(main.img,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(255, 255,255), 1, 1, 5);

      cvLine(main.img,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(255, 255, 255), 1, 1, 5);
      cvCircle(main.img,cvPoint(xTankaAbs, yTankaAbs),200,CV_RGB(255, 255, 255), 2, 2, 5);

      // стенки коробки
      if (xTankaAbs < xPoligona - radius || xTankaAbs > xPoligona + radius || yTankaAbs < yPoligona - radius || yTankaAbs > yPoligona + radius ){
          velositi = 0;
         // cvPutText(main.img,"YOU CRASHED !!!!",cvPoint(150,250),cvFont(6,10) ,CvScalar.BLACK);
          xTankaAbs = xPoligona + xTanka;
          yTankaAbs = yPoligona + yTanka;
          // main.rabota = false;
      }



      // изменение направления машинки и ее координат
      if (velositi >= 0){
          fi = fi - (double)angleVelosity/2;
      } else {
          fi = fi + (double)angleVelosity/2;

      }


      xTankaAbs = xTankaAbs + (int)(velositi*Math.cos(fi/radian));
      yTankaAbs = yTankaAbs + (int)(velositi*Math.sin(fi/radian));
      odo =  odo +  (int)Math.sqrt(Math.pow(xTankaAbs-xTankaPred, 2) + Math.pow(yTankaAbs-yTankaPred, 2));
      sumUglovoeUsk = sumUglovoeUsk + (int)(radiusPovarota*Math.pow(angleVelosity,2))/50 ;
      sumLinUsk = sumLinUsk + Math.abs(velositi - oldVelosity);
      oldVelosity = velositi;
      if (angleVelosity != 0){
          radiusPovarota = Math.abs((int) (velositi / angleVelosity));
      }   else radiusPovarota = 0;



      xTankaPred = xTankaAbs;
      yTankaPred = yTankaAbs;
      // отправка координат в мэйн
      main.odo = -240 + odo/100;
      main.xTanka = xTankaAbs;
      main.yTanka = yTankaAbs;
      main.radiuspovorota = radiusPovarota;
      main.sunOtnUscorenie = sumUglovoeUsk + sumLinUsk ;
      // Рисование машинки и вектора скорости
      
      cvLine(main.img,cvPoint(xTankaAbs, yTankaAbs),cvPoint(xTankaAbs,yTankaAbs),CV_RGB(0, 0, 0), 1, 1, 5);

      // рисуем танчик  и трек
      // передний габарит
      cvLine(main.img,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
                     ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
                     ,CV_RGB(0, 0, 0), 1, 1, 5);
      // задний габарит
      cvLine(main.img,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(0, 0, 0), 1, 1, 5);
      // левый габарит
      cvLine(main.img,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(0, 0, 0), 1, 1, 5);
      // правый габарит
      cvLine(main.img,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(0, 0, 0), 1, 1, 5);
      // пушка
      cvLine(main.img,cvPoint(xTankaAbs, yTankaAbs),cvPoint(xTankaAbs + (int)(vectorSkorosty*Math.cos(fi/radian))
              ,yTankaAbs + (int)(vectorSkorosty*Math.sin(fi/radian))),CV_RGB(0, 0, 0), 2, 2, 5);
      // башенка
      cvCircle(main.img,cvPoint(xTankaAbs, yTankaAbs),200,CV_RGB(0, 0, 0), 2, 2, 5);
      // следы гусениц  позади
      cvLine(main.img,cvPoint(xTankaAbs - (int)((machineDiagonal)*Math.cos(fi/radian -machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,CV_RGB(100, 100, 100), 2, 2, 5);
      cvLine(main.img,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian +machineAngle)),yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,cvPoint(xTankaAbs - (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs - (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(100, 100, 100), 2, 2, 5);
      // следы гусениц впереди
      cvLine(main.img,cvPoint(xTankaAbs + (int)((machineDiagonal)*Math.cos(fi/radian -machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian -machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian - machineAngle)))
              ,CV_RGB(100, 100, 100), 2, 2, 5);
      cvLine(main.img,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian +machineAngle)),yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,cvPoint(xTankaAbs + (int)(machineDiagonal*Math.cos(fi/radian + machineAngle)) ,yTankaAbs + (int)(machineDiagonal*Math.sin(fi/radian + machineAngle)))
              ,CV_RGB(100, 100, 100), 2, 2, 5);


  }

}
